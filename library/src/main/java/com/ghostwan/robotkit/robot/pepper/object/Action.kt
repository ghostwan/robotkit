package com.ghostwan.robotkit.robot.pepper.`object`

/**
 * Robot actions
 */
enum class Action {
    TALKING, LISTENING, MOVING
}